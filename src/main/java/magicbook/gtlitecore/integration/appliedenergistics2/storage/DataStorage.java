package magicbook.gtlitecore.integration.appliedenergistics2.storage;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;

public class DataStorage {
    public static final DataStorage EMPTY = new DataStorage();

    public NBTTagList stackKeys;
    public long[] stackAmounts;
    public long itemCount;

    public DataStorage() {
        stackKeys = new NBTTagList();
        stackAmounts = new long[0];
        itemCount = 0;
    }

    public DataStorage(NBTTagList stackKeys, long[] stackAmounts, long itemCount) {
        this.stackKeys = stackKeys;
        this.stackAmounts = stackAmounts;
        this.itemCount = itemCount;
    }

//    public NBTTagCompound toNbt() {
//        NBTTagCompound nbt = new NBTTagCompound();
//        nbt.setTag(DISKCellInventory.STACK_KEYS, stackKeys);
//        nbt(DISKCellInventory.STACK_AMOUNTS, stackAmounts);
//        if (itemCount != 0)
//            nbt.putLong(DISKCellInventory.ITEM_COUNT_TAG, itemCount);
//
//        return nbt;
//    }
//
//    public static DataStorage fromNbt(NBTTagCompound nbt) {
//        long itemCount = 0;
//        NBTTagList stackKeys = nbt.getList(DISKCellInventory.STACK_KEYS, Tag.TAG_COMPOUND);
//        long[] stackAmounts = nbt.getLongArray(DISKCellInventory.STACK_AMOUNTS);
//        if (nbt.contains(DISKCellInventory.ITEM_COUNT_TAG))
//            itemCount = nbt.getLong(DISKCellInventory.ITEM_COUNT_TAG);
//
//        return new DataStorage(stackKeys, stackAmounts, itemCount);
//    }
}
