package magicbook.gtlitecore.integration.appliedenergistics2.storage;

import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import magicbook.gtlitecore.GTLiteCore;
import mcp.MethodsReturnNonnullByDefault;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import net.minecraft.world.storage.MapStorage;
import net.minecraft.world.storage.WorldSavedData;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
@MethodsReturnNonnullByDefault
@Mod.EventBusSubscriber(modid = GTLiteCore.MODID)
public class StorageManager extends WorldSavedData {

    private static final String STORAGE_DATA = "StorageWorldSavedData";

    public static volatile StorageManager instance;

    private final Int2ObjectMap<Object> storages;

    public StorageManager() {
        super(STORAGE_DATA);
        this.storages = new Int2ObjectOpenHashMap<>();
        this.markDirty();
    }

    private static void initialize(World world) {
        MapStorage storage = world.getMapStorage();
        if (storage != null) {
            instance = (StorageManager) world.getMapStorage().getOrLoadData(StorageManager.class, STORAGE_DATA);
            if (instance == null) {
                instance = new StorageManager();
                storage.setData(STORAGE_DATA, instance);
            }
            instance.markDirty();
        }
    }

    @SubscribeEvent
    public static void onWorldLoad(WorldEvent.Load event) {
        if (!event.getWorld().isRemote && event.getWorld().provider.getDimension() == 0) {
            initialize(event.getWorld());
        }
    }

    @Override
    public void readFromNBT(NBTTagCompound nbt) {

    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        return null;
    }
}
