package magicbook.gtlitecore.integration.appliedenergistics2.registration.registry.definition;

public interface IEnabled {

    boolean isEnabled();
}
