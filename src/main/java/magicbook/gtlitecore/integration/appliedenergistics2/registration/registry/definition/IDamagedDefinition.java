package magicbook.gtlitecore.integration.appliedenergistics2.registration.registry.definition;

import appeng.api.definitions.IItemDefinition;
import magicbook.gtlitecore.integration.appliedenergistics2.registration.registry.rendering.IModelProvider;
import net.minecraft.item.ItemStack;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface IDamagedDefinition<T extends IItemDefinition, U extends IModelProvider> extends IRegisterDefinition<T> {

    Collection<U> getEntries();

    @Nullable
    U getType(ItemStack stack);
}
