package magicbook.gtlitecore.integration.appliedenergistics2.registration.registry.rendering;

import magicbook.gtlitecore.integration.appliedenergistics2.registration.registry.definition.IEnabled;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;

public interface IModelProvider extends IEnabled {

    ModelResourceLocation getModel();
}
