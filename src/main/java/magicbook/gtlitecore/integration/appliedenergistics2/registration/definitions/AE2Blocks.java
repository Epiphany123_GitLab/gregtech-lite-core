package magicbook.gtlitecore.integration.appliedenergistics2.registration.definitions;

import appeng.api.definitions.ITileDefinition;
import appeng.block.crafting.ItemCraftingStorage;
import appeng.bootstrap.definitions.TileEntityDefinition;
import co.neeve.nae2.common.features.Features;
import co.neeve.nae2.common.features.subfeatures.DenseCellFeatures;
import lombok.Getter;
import lombok.experimental.Accessors;
import magicbook.gtlitecore.integration.appliedenergistics2.registration.registry.AE2Registry;
import magicbook.gtlitecore.integration.appliedenergistics2.client.rendering.ExtremeCraftingCubeRendering;
import magicbook.gtlitecore.integration.appliedenergistics2.blocks.BlockExtremeCraftingUnit;
import magicbook.gtlitecore.integration.appliedenergistics2.tiles.TileExtremeCraftingUnit;

import static magicbook.gtlitecore.integration.appliedenergistics2.blocks.BlockExtremeCraftingUnit.ExtremeCraftingUnitType.COPROCESSOR_1024X;
import static magicbook.gtlitecore.integration.appliedenergistics2.blocks.BlockExtremeCraftingUnit.ExtremeCraftingUnitType.COPROCESSOR_256X;
import static magicbook.gtlitecore.integration.appliedenergistics2.blocks.BlockExtremeCraftingUnit.ExtremeCraftingUnitType.COPROCESSOR_4096X;
import static magicbook.gtlitecore.integration.appliedenergistics2.blocks.BlockExtremeCraftingUnit.ExtremeCraftingUnitType.STORAGE_131072K;
import static magicbook.gtlitecore.integration.appliedenergistics2.blocks.BlockExtremeCraftingUnit.ExtremeCraftingUnitType.STORAGE_32768K;
import static magicbook.gtlitecore.integration.appliedenergistics2.blocks.BlockExtremeCraftingUnit.ExtremeCraftingUnitType.STORAGE_65536K;

@Getter @Accessors(fluent = true)
public class AE2Blocks {
    //  Please pre-init your block here like `private final ITileDefinition blockName;`.
    private final ITileDefinition storageCrafting32768x;

    private final ITileDefinition storageCrafting65536x;

    private final ITileDefinition storageCrafting131072x;

    private final ITileDefinition coprocessor256x;

    private final ITileDefinition coprocessor1024x;

    private final ITileDefinition coprocessor4096x;

    public AE2Blocks(AE2Registry registry) {
        //  Please register your block here, just use `addBlock()` method in AE2Registry.
        this.storageCrafting32768x = registry.addBlock("storage_crafting_32768k", () -> new BlockExtremeCraftingUnit(STORAGE_32768K))
                .tileEntity(new TileEntityDefinition(TileExtremeCraftingUnit.class, "crafting_storage"))
                .rendering(new ExtremeCraftingCubeRendering("storage_crafting_32768k", STORAGE_32768K))
                .useCustomItemModel()
                .item(ItemCraftingStorage::new)
                .features(DenseCellFeatures.DENSE_CPU_STORAGE_UNITS).build();

        this.storageCrafting65536x = registry.addBlock("storage_crafting_65536k", () -> new BlockExtremeCraftingUnit(STORAGE_65536K))
                .tileEntity(new TileEntityDefinition(TileExtremeCraftingUnit.class, "crafting_storage"))
                .rendering(new ExtremeCraftingCubeRendering("storage_crafting_65536k", STORAGE_65536K))
                .useCustomItemModel()
                .item(ItemCraftingStorage::new)
                .features(DenseCellFeatures.DENSE_CPU_STORAGE_UNITS).build();

        this.storageCrafting131072x = registry.addBlock("storage_crafting_131072k", () -> new BlockExtremeCraftingUnit(STORAGE_131072K))
                .tileEntity(new TileEntityDefinition(TileExtremeCraftingUnit.class, "crafting_storage"))
                .rendering(new ExtremeCraftingCubeRendering("storage_crafting_131072k", STORAGE_131072K))
                .useCustomItemModel()
                .item(ItemCraftingStorage::new)
                .features(DenseCellFeatures.DENSE_CPU_STORAGE_UNITS).build();

        this.coprocessor256x = registry.addBlock("coprocessor_256x", () -> new BlockExtremeCraftingUnit(COPROCESSOR_256X))
                .tileEntity(new TileEntityDefinition(TileExtremeCraftingUnit.class, "crafting_storage"))
                .rendering(new ExtremeCraftingCubeRendering("coprocessor_256x", COPROCESSOR_256X))
                .useCustomItemModel()
                .features(Features.DENSE_CPU_COPROCESSORS).build();

        this.coprocessor1024x = registry.addBlock("coprocessor_1024x", () -> new BlockExtremeCraftingUnit(COPROCESSOR_1024X))
                .tileEntity(new TileEntityDefinition(TileExtremeCraftingUnit.class, "crafting_storage"))
                .rendering(new ExtremeCraftingCubeRendering("coprocessor_1024x", COPROCESSOR_1024X))
                .useCustomItemModel()
                .features(Features.DENSE_CPU_COPROCESSORS).build();

        this.coprocessor4096x = registry.addBlock("coprocessor_4096x", () -> new BlockExtremeCraftingUnit(COPROCESSOR_4096X))
                .tileEntity(new TileEntityDefinition(TileExtremeCraftingUnit.class, "crafting_storage"))
                .rendering(new ExtremeCraftingCubeRendering("coprocessor_4096x", COPROCESSOR_4096X))
                .useCustomItemModel()
                .features(Features.DENSE_CPU_COPROCESSORS).build();
    }
}
