package magicbook.gtlitecore.integration.appliedenergistics2.registration.definitions;

import appeng.api.AEApi;
import appeng.api.config.Upgrades;
import appeng.api.definitions.IItemDefinition;
import appeng.bootstrap.components.IPostInitComponent;
import co.neeve.nae2.common.features.Features;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import lombok.Getter;
import lombok.experimental.Accessors;
import magicbook.gtlitecore.integration.appliedenergistics2.items.cells.qc.QuantumCell;
import magicbook.gtlitecore.integration.appliedenergistics2.items.cells.qc.QuantumFluidCell;
import magicbook.gtlitecore.integration.appliedenergistics2.items.cells.qc.QuantumItemCell;
import magicbook.gtlitecore.integration.appliedenergistics2.items.cells.sc.SingularityCell;
import magicbook.gtlitecore.integration.appliedenergistics2.items.cells.sc.SingularityFluidCell;
import magicbook.gtlitecore.integration.appliedenergistics2.items.cells.sc.SingularityItemCell;
import magicbook.gtlitecore.integration.appliedenergistics2.registration.registry.AE2Registry;
import magicbook.gtlitecore.integration.appliedenergistics2.registration.registry.definition.IRegisterDefinition;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Getter
@Accessors(fluent = true)
public class AE2Items implements IRegisterDefinition<IItemDefinition> {

    private final Object2ObjectOpenHashMap<String, IItemDefinition> itemIdList = new Object2ObjectOpenHashMap<>();

    //  Please pre-init item here, like: `private final IItemDefinition itemName;`.
    private final IItemDefinition storageCellQuantum;
    private final IItemDefinition storageCellFluidQuantum;
    private final IItemDefinition storageCellSingularity;
    private final IItemDefinition storageCellFluidSingularity;


    public AE2Items(AE2Registry registry) {

        //  Please init item here like `this.itemName = this.registerItem()`.
        this.storageCellQuantum = registerItem(registry.addItem("quantum_cell", () ->
                        new QuantumItemCell(Long.MAX_VALUE / 8))
                .features(Features.DENSE_CELLS)
                .build());

        this.storageCellFluidQuantum = registerItem(registry.addItem("quantum_cell_fluid", () ->
                        new QuantumFluidCell(Long.MAX_VALUE / 8000))
                .features(Features.DENSE_CELLS)
                .build());

        this.storageCellSingularity = registerItem(registry.addItem("singularity_cell", SingularityItemCell::new)
                .features(Features.DENSE_CELLS)
                .build());

        this.storageCellFluidSingularity = registerItem(registry.addItem("singularity_cell_fluid", SingularityFluidCell::new)
                .features(Features.DENSE_CELLS)
                .build());

        //  Add Post-Init Bootstrap Component for Items.
        registry.addBootstrapComponent((IPostInitComponent) c -> {
            var items = AEApi.instance().definitions().items();
            var cellDef = items.cell1k();
            var fluidCellDef = items.fluidCell1k();

            //  Please check Features if it is enabled and mirrored cell upgrades here (if you need),
            //  just like `mirrorCellUpgrades(cellDef, this.itemName)`, also you can use IItemDefinition list.
            if (Features.DENSE_CELLS.isEnabled() && cellDef.isEnabled()) {
                mirrorCellUpgrades(cellDef, new IItemDefinition[] {
                        this.storageCellQuantum,
                        this.storageCellFluidQuantum
                });

                AEApi.instance().registries().cell().addCellHandler(new QuantumCell.Handler());
                AEApi.instance().registries().cell().addCellHandler(new SingularityCell.Handler());
            }

        });
    }

    private IItemDefinition registerItem(IItemDefinition item) {
        this.itemIdList.put(item.identifier(), item);
        return item;
    }

    private static void mirrorCellUpgrades(IItemDefinition definition, IItemDefinition[] definitions) {
        var supported = new HashMap<Upgrades, Integer>();
        Arrays.stream(Upgrades.values()).forEach(upgrade -> upgrade.getSupported().entrySet().stream()
                .filter(d -> definition.isSameAs(d.getKey()))
                .map(Map.Entry::getValue)
                .findFirst()
                .ifPresent(value -> supported.put(upgrade, value)));
        Arrays.stream(definitions).forEach(def -> supported.forEach((key, value) -> key.registerItem(def, value)));
    }

    @Override
    public Optional<IItemDefinition> getById(String id) {
        return Optional.ofNullable(this.itemIdList.getOrDefault(id, null));
    }

}
