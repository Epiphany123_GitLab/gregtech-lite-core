package magicbook.gtlitecore.integration.appliedenergistics2.tiles;

import appeng.tile.crafting.TileCraftingStorageTile;
import co.neeve.nae2.common.interfaces.IDenseCoProcessor;
import lombok.NoArgsConstructor;
import magicbook.gtlitecore.integration.appliedenergistics2.blocks.BlockExtremeCraftingUnit;
import net.minecraft.block.Block;
import net.minecraft.item.ItemStack;

import javax.annotation.Nullable;

@NoArgsConstructor
public class TileExtremeCraftingUnit extends TileCraftingStorageTile implements IDenseCoProcessor {

    @Override
    protected ItemStack getItemFromTile(Object obj) {
        BlockExtremeCraftingUnit unit = getBlock();
        return unit != null ? unit.getType().getBlock().maybeStack(1).orElse(ItemStack.EMPTY) : ItemStack.EMPTY;
    }

    @Override
    public boolean isAccelerator() {
        return false;
    }

    @Override
    public boolean isStorage() {
        BlockExtremeCraftingUnit unit = getBlock();
        return unit != null && unit.getType().getBytes() > 0;
    }

    @Override
    public int getStorageBytes() {
        BlockExtremeCraftingUnit unit = getBlock();
        return unit != null ? unit.getType().getBytes() : 0;
    }

    @Override
    public int getAccelerationFactor() {
        BlockExtremeCraftingUnit unit = getBlock();
        return unit != null ? unit.getType().getAccelFactor() : 0;
    }

    @Nullable
    public BlockExtremeCraftingUnit getBlock() {
        if (world != null && !notLoaded() && !isInvalid()) {
            Block block = getWorld().getBlockState(getPos()).getBlock();
            if (block instanceof BlockExtremeCraftingUnit unit) {
                return unit;
            }
        }
        return null;
    }
}
