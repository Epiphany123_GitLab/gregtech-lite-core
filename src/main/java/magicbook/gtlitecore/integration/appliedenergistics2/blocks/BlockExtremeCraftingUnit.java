package magicbook.gtlitecore.integration.appliedenergistics2.blocks;

import appeng.api.definitions.ITileDefinition;
import appeng.block.crafting.BlockCraftingUnit;
import lombok.AllArgsConstructor;
import lombok.Getter;
import magicbook.gtlitecore.GTLiteCore;

import static magicbook.gtlitecore.api.GTLiteValues.KILO_SCALAR;

@Getter
public class BlockExtremeCraftingUnit extends BlockCraftingUnit {

    public final ExtremeCraftingUnitType type;

    public BlockExtremeCraftingUnit(ExtremeCraftingUnitType type) {
        super(null);
        this.type = type;
    }

    @AllArgsConstructor @Getter
    public enum ExtremeCraftingUnitType {
        STORAGE_32768K(32768 * KILO_SCALAR, 0) {
            @Override
            public ITileDefinition getBlock() {
                return GTLiteCore.getAE2RegisterManager().block().storageCrafting32768x();
            }
        },
        STORAGE_65536K(65536 * KILO_SCALAR, 0) {
            @Override
            public ITileDefinition getBlock() {
                return GTLiteCore.getAE2RegisterManager().block().storageCrafting65536x();
            }
        },
        STORAGE_131072K(131072 * KILO_SCALAR, 0) {
            @Override
            public ITileDefinition getBlock() {
                return GTLiteCore.getAE2RegisterManager().block().storageCrafting131072x();
            }
        },
        COPROCESSOR_256X(0, 256) {
            @Override
            public ITileDefinition getBlock() {
                return GTLiteCore.getAE2RegisterManager().block().coprocessor256x();
            }
        },
        COPROCESSOR_1024X(0, 1024) {
            @Override
            public ITileDefinition getBlock() {
                return GTLiteCore.getAE2RegisterManager().block().coprocessor1024x();
            }
        },
        COPROCESSOR_4096X(0, 4096) {
            @Override
            public ITileDefinition getBlock() {
                return GTLiteCore.getAE2RegisterManager().block().coprocessor4096x();
            }
        };

        public final int bytes;

        public final int accelFactor;

        public abstract ITileDefinition getBlock();
    }
}
