package magicbook.gtlitecore.integration.appliedenergistics2.client.utils;

import appeng.api.config.IncludeExclude;
import appeng.api.storage.ICellInventory;
import appeng.api.storage.ICellInventoryHandler;
import appeng.api.storage.channels.IFluidStorageChannel;
import appeng.api.storage.channels.IItemStorageChannel;
import appeng.api.storage.data.IAEFluidStack;
import appeng.api.storage.data.IAEItemStack;
import appeng.api.storage.data.IAEStack;
import appeng.api.storage.data.IItemList;
import appeng.core.AEConfig;
import appeng.core.localization.GuiText;
import appeng.fluids.items.FluidDummyItem;
import appeng.fluids.util.AEFluidStack;
import appeng.util.ReadableNumberConverter;
import appeng.util.item.AEItemStack;
import net.minecraft.client.Minecraft;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fluids.FluidUtil;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.oredict.OreDictionary;
import org.lwjgl.input.Keyboard;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

@SideOnly(Side.CLIENT)
public class AE2ClientHelper {
    private AE2ClientHelper() {
    }

//    public <T extends IAEStack<T>> void addCellInformation(ICellInventoryHandler<T> handler, List<String> lines) {
//        if (handler != null) {
//            ICellInventory<?> cellInventory = handler.getCellInv();
//            if (cellInventory != null) {
//                lines.add(cellInventory.getUsedBytes() + " " + GuiText.Of.getLocal() + ' ' + cellInventory.getTotalBytes() + ' ' + GuiText.BytesUsed.getLocal());
//                lines.add(cellInventory.getStoredItemTypes() + " " + GuiText.Of.getLocal() + ' ' + cellInventory.getTotalItemTypes() + ' ' + GuiText.Types.getLocal());
//            }
//
//            IItemList<?> itemList = cellInventory.getChannel().createList();
//            if (handler.isPreformatted()) {
//                String list = (handler.getIncludeExcludeMode() == IncludeExclude.WHITELIST ? GuiText.Included : GuiText.Excluded).getLocal();
//                if (handler.isFuzzy()) {
//                    lines.add("[" + GuiText.Partitioned.getLocal() + "] - " + list + ' ' + GuiText.Fuzzy.getLocal());
//                } else {
//                    lines.add("[" + GuiText.Partitioned.getLocal() + "] - " + list + ' ' + GuiText.Precise.getLocal());
//                }
//
//                if (handler.isSticky()) {
//                    lines.add(GuiText.Sticky.getLocal());
//                }
//
//                if (Minecraft.getMinecraft().gameSettings.advancedItemTooltips || Keyboard.isKeyDown(42) || Keyboard.isKeyDown(54)) {
//                    IItemHandler inv = cellInventory.getConfigInventory();
//                    cellInventory.getAvailableItems(itemList);
//
//                    for(int i = 0; i < inv.getSlots(); ++i) {
//                        ItemStack is = inv.getStackInSlot(i);
//                        if (!is.isEmpty()) {
//                            if (!(cellInventory.getChannel() instanceof IItemStorageChannel)) {
//                                if (cellInventory.getChannel() instanceof IFluidStorageChannel) {
//                                    AEFluidStack ais;
//                                    if (is.getItem() instanceof FluidDummyItem) {
//                                        ais = AEFluidStack.fromFluidStack(((FluidDummyItem)is.getItem()).getFluidStack(is));
//                                    } else {
//                                        ais = AEFluidStack.fromFluidStack(FluidUtil.getFluidContained(is));
//                                    }
//
//                                    IAEFluidStack stocked = (IAEFluidStack)itemList.findPrecise(ais);
//                                    lines.add("[" + is.getDisplayName() + "]: " + (stocked == null ? "0" : this.fluidStackSize(stocked.getStackSize())));
//                                }
//                            } else {
//                                AEItemStack ais;
//                                if (!handler.isFuzzy()) {
//                                    ais = AEItemStack.fromItemStack(is);
//                                    IAEItemStack stocked = (IAEItemStack)itemList.findPrecise(ais);
//                                    lines.add("[" + is.getDisplayName() + "]: " + (stocked == null ? "0" : ReadableNumberConverter.INSTANCE.toWideReadableForm(stocked.getStackSize())));
//                                } else {
//                                    ais = AEItemStack.fromItemStack(is);
//                                    Collection<IAEItemStack> stocked = itemList.findFuzzy(ais, handler.getCellInv().getFuzzyMode());
//                                    int[] ids = OreDictionary.getOreIDs(is);
//                                    long size = 0L;
//
//                                    IAEItemStack ist;
//                                    for(Iterator var14 = stocked.iterator(); var14.hasNext(); size += ist.getStackSize()) {
//                                        ist = (IAEItemStack)var14.next();
//                                    }
//
//                                    if (is.getItem().isDamageable()) {
//                                        lines.add("[" + is.getDisplayName() + "]: " + size);
//                                    } else if (ids.length > 0) {
//                                        StringBuilder sb = new StringBuilder();
//
//                                        for (int j : ids) {
//                                            sb.append(OreDictionary.getOreName(j)).append(", ");
//                                        }
//
//                                        lines.add("[{" + sb.substring(0, sb.length() - 2) + "}]: " + ReadableNumberConverter.INSTANCE.toWideReadableForm(size));
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
//            } else {
//                if (!AEConfig.instance().showCellContentsPreview()) {
//                    return;
//                }
//
//                if (Minecraft.getMinecraft().gameSettings.advancedItemTooltips || Keyboard.isKeyDown(42) || Keyboard.isKeyDown(54)) {
//                    cellInventory.getAvailableItems(itemList);
//
//                    for (IAEStack<?> iaeStack : itemList) {
//                        IAEStack<?> s = iaeStack;
//                        if (s instanceof IAEItemStack) {
//                            lines.add(((IAEItemStack) s).getDefinition().getDisplayName() + ": " + ReadableNumberConverter.INSTANCE.toWideReadableForm(s.getStackSize()));
//                        } else if (s instanceof IAEFluidStack) {
//                            lines.add(((IAEFluidStack) s).getFluidStack().getLocalizedName() + ": " + this.fluidStackSize(s.getStackSize()));
//                        }
//                    }
//                }
//            }
//
//        }
//    }
//
//    private String fluidStackSize(long size) {
//        String unit;
//        if (size >= 1000L) {
//            unit = "B";
//        } else {
//            unit = "mB";
//        }
//
//        int log = (int)Math.floor(Math.log10((double)size)) / 2;
//        int index = Math.max(0, Math.min(3, log));
//        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
//        symbols.setDecimalSeparator('.');
//        DecimalFormat format = new DecimalFormat(NUMBER_FORMATS[index]);
//        format.setDecimalFormatSymbols(symbols);
//        format.setRoundingMode(RoundingMode.DOWN);
//        String formatted = format.format((double)size / 1000.0);
//        return formatted.concat(unit);
//    }
}
