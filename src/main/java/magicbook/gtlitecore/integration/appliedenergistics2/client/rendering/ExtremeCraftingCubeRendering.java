package magicbook.gtlitecore.integration.appliedenergistics2.client.rendering;

import appeng.block.crafting.BlockCraftingUnit;
import appeng.bootstrap.BlockRenderingCustomizer;
import appeng.bootstrap.IBlockRendering;
import appeng.bootstrap.IItemRendering;
import magicbook.gtlitecore.integration.appliedenergistics2.blocks.BlockExtremeCraftingUnit;
import magicbook.gtlitecore.integration.appliedenergistics2.client.model.ExtremeCraftingCubeModel;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.HashMap;
import java.util.Map;

import static magicbook.gtlitecore.api.utils.GTLiteUtility.gtliteId;

public class ExtremeCraftingCubeRendering extends BlockRenderingCustomizer {
    private final String registryName;
    private final BlockExtremeCraftingUnit.ExtremeCraftingUnitType type;

    public ExtremeCraftingCubeRendering(String registryName, BlockExtremeCraftingUnit.ExtremeCraftingUnitType type) {
        this.registryName = registryName;
        this.type = type;
    }

    @SideOnly(Side.CLIENT)
    public void customize(IBlockRendering rendering, IItemRendering itemRendering) {
        ResourceLocation baseName = gtliteId(registryName);
        ModelResourceLocation defaultModel = new ModelResourceLocation(baseName, "normal");
        String builtInName = "models/block/crafting/" + registryName + "/builtin";
        ModelResourceLocation builtInModelName = new ModelResourceLocation(gtliteId(builtInName), "normal");
        rendering.builtInModel(builtInName, new ExtremeCraftingCubeModel(type));
        rendering.stateMapper(block -> mapState(block, defaultModel, builtInModelName));
        rendering.modelCustomizer((loc, model) -> model);
    }

    @SideOnly(Side.CLIENT)
    private Map<IBlockState, ModelResourceLocation> mapState(Block block, ModelResourceLocation defaultModel, ModelResourceLocation formedModel) {
        Map<IBlockState, ModelResourceLocation> result = new HashMap<>();

        for (IBlockState state : block.getBlockState().getValidStates()) {
            if (state.getValue(BlockCraftingUnit.FORMED)) {
                result.put(state, formedModel);
            } else {
                result.put(state, defaultModel);
            }
        }

        return result;
    }
}
