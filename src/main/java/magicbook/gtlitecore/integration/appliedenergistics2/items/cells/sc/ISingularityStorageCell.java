package magicbook.gtlitecore.integration.appliedenergistics2.items.cells.sc;

import appeng.api.storage.ICellWorkbenchItem;
import appeng.api.storage.IStorageChannel;
import appeng.api.storage.data.IAEStack;
import net.minecraft.item.ItemStack;

import javax.annotation.Nonnull;

public interface ISingularityStorageCell<T extends IAEStack<T>> extends ICellWorkbenchItem {
    boolean isBlackListed(@Nonnull ItemStack var1, @Nonnull T var2);

    boolean storableInStorageCell();

    boolean isStorageCell(@Nonnull ItemStack var1);

    double getIdleDrain();

    @Nonnull
    IStorageChannel<T> getChannel();
}
