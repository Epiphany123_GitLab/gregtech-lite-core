package magicbook.gtlitecore.integration.appliedenergistics2.items.cells.sc;

import appeng.api.storage.IStorageChannel;
import appeng.api.storage.channels.IItemStorageChannel;
import appeng.api.storage.data.IAEItemStack;
import appeng.core.Api;
import org.jetbrains.annotations.NotNull;

public class SingularityItemCell extends SingularityCell<IAEItemStack> {

    public SingularityItemCell() {
        super();
    }

    @Override
    public @NotNull IStorageChannel<IAEItemStack> getChannel() {
        return Api.INSTANCE.storage().getStorageChannel(IItemStorageChannel.class);
    }
}
