package magicbook.gtlitecore.integration.appliedenergistics2.items.cells.ic;

import appeng.api.config.IncludeExclude;
import appeng.api.storage.ICellInventory;
import appeng.api.storage.ICellInventoryHandler;
import appeng.api.storage.IMEInventory;
import appeng.api.storage.IStorageChannel;
import appeng.api.storage.data.IAEStack;
import appeng.me.storage.MEInventoryHandler;
import org.jetbrains.annotations.Nullable;

public class InfinityCellInventoryHandler<T extends IAEStack<T>> extends MEInventoryHandler<T> implements ICellInventoryHandler<T> {

    public InfinityCellInventoryHandler(IMEInventory i, IStorageChannel channel) {
        super(i, channel);
    }

    @Nullable
    @Override
    public ICellInventory<T> getCellInv() {
        return null;
    }

    @Override
    public boolean isPreformatted() {
        return false;
    }

    @Override
    public boolean isFuzzy() {
        return false;
    }

    @Override
    public IncludeExclude getIncludeExcludeMode() {
        return null;
    }
}
