package magicbook.gtlitecore.integration.appliedenergistics2.items.cells.qc;

import appeng.api.storage.channels.IItemStorageChannel;
import appeng.api.storage.data.IAEItemStack;
import appeng.core.Api;
import net.minecraft.item.ItemStack;
import org.jetbrains.annotations.NotNull;

/**
 * Quantum Item Cell
 *
 * @author Magic_Sweepy
 *
 * <p>
 *     This class is used for Quantum Storage Cell.
 * </p>
 */
public class QuantumItemCell extends QuantumCell<IAEItemStack> {

    public QuantumItemCell(long bytes) {
        super(bytes);
    }

    @NotNull
    @Override
    public IItemStorageChannel getChannel() {
        return Api.INSTANCE.storage().getStorageChannel(IItemStorageChannel.class);
    }

    @Override
    public int getTotalTypes(@NotNull ItemStack cellItem) {
        return 1;
    }
}

