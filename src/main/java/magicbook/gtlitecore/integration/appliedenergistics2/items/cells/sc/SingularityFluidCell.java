package magicbook.gtlitecore.integration.appliedenergistics2.items.cells.sc;

import appeng.api.storage.IStorageChannel;
import appeng.api.storage.channels.IFluidStorageChannel;
import appeng.api.storage.data.IAEFluidStack;
import appeng.core.Api;
import org.jetbrains.annotations.NotNull;

public class SingularityFluidCell extends SingularityCell<IAEFluidStack> {

    public SingularityFluidCell() {
        super();
    }

    @Override
    public @NotNull IStorageChannel<IAEFluidStack> getChannel() {
        return Api.INSTANCE.storage().getStorageChannel(IFluidStorageChannel.class);
    }
}
