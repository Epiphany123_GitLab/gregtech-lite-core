package magicbook.gtlitecore.integration.appliedenergistics2.items.cells.qc;

import appeng.api.storage.channels.IFluidStorageChannel;
import appeng.api.storage.data.IAEFluidStack;
import appeng.core.Api;
import net.minecraft.item.ItemStack;
import org.jetbrains.annotations.NotNull;

public class QuantumFluidCell extends QuantumCell<IAEFluidStack> {

    public QuantumFluidCell(long bytes) {
        super(bytes);
    }

    @NotNull
    @Override
    public IFluidStorageChannel getChannel() {
        return Api.INSTANCE.storage().getStorageChannel(IFluidStorageChannel.class);
    }

    @Override
    public int getTotalTypes(@NotNull ItemStack cellItem) {
        return 1;
    }
}
