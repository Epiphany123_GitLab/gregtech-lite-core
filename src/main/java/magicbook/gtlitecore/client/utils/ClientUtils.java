package magicbook.gtlitecore.client.utils;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BlockModelRenderer;
import net.minecraft.client.renderer.BlockModelShapes;
import net.minecraft.client.renderer.BlockRendererDispatcher;
import net.minecraft.client.renderer.ItemModelMesher;
import net.minecraft.client.renderer.ItemRenderer;
import net.minecraft.client.renderer.RenderItem;
import net.minecraft.client.renderer.block.model.IBakedModel;
import net.minecraft.client.renderer.block.model.ModelManager;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class ClientUtils {

    private ClientUtils() {
    }

    public static Minecraft mc() {
        return Minecraft.getMinecraft();
    }

    public static ItemRenderer itemRenderer() {
        return mc().getItemRenderer();
    }

    public static RenderItem renderItem() {
        return mc().getItemRenderer().itemRenderer;
    }

    public static BlockRendererDispatcher blockRenderer() {
        return mc().blockRenderDispatcher;
    }

    public static BlockModelRenderer blockModelRenderer() {
        return blockRenderer().getBlockModelRenderer();
    }

    public static BlockModelShapes blockModelShapes() {
        return blockRenderer().getBlockModelShapes();
    }

    public static ItemModelMesher itemModelMesher() {
        return renderItem().getItemModelMesher();
    }

    public static ModelManager modelManager() {
        return blockModelShapes().getModelManager();
    }

    public static IBakedModel missingModel() {
        return modelManager().getMissingModel();
    }
}
